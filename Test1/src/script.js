import * as THREE from "three";


// Canvas
const canvas = document.querySelector("canvas.webgl");



// Scene
const scene = new THREE.Scene();

// Object
// Sphere
// const geometry = new THREE.SphereGeometry(0.5, 32, 32)
// Cube
const geometry = new THREE.BoxGeometry(1, 1, 1);
const material = new THREE.MeshStandardMaterial({ color: 0xff0000 });
const mesh = new THREE.Mesh(geometry, material);
const colorPicker = document.getElementById('color-picker');
colorPicker.addEventListener('change', (event) => {
  const color = event.target.value;
  mesh.material.color.set(color);
});
scene.add(mesh);
// Position de l'objet dans l'espace (x,y,z)
mesh.position.set(1,2,0)

// Scales
mesh.scale.x=1
mesh.scale.y=1
mesh.scale.z=1
// mesh.scale.set(x,y,z) donne exactement le même résultat.

// Plan
// const plane = new THREE.Mesh(mesh);
// scene.add( plane );

// Axes helper
const axesHelper = new THREE.AxesHelper(20)
scene.add(axesHelper)

// Light
const ambientLight = new THREE.AmbientLight(0xffffff, 0.5);
scene.add(ambientLight);

const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
directionalLight.position.set(10, 10, 10);
scene.add(directionalLight);

// Sizes
const sizes = {
  width: 800,
  height: 600,
};

// Camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);

camera.position.x = 1;
camera.position.y = 1;
// Si on veut s'éloigner de l'objet, on peut augmenter position.z, et inversement pour s'en éloigner
camera.position.z = 3;
// camera.position.set(x,y,z) donne exactement le même résultat.

scene.add(camera);

// Renderer
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
});
renderer.setSize(sizes.width, sizes.height);
renderer.setClearColor(0x444444);

// Time
let time = Date.now();


// Pause 
const pauseBtn = document.querySelector('#pauseBtn');
let isPaused = pauseBtn.dataset.paused === 'true';
pauseBtn.dataset.paused = 'true'; // Le jeu est maintenant en pause
if (isPaused) {
    pauseBtn.innerText = 'Resume';
  } else {
    pauseBtn.innerText = 'Pause';
  }
 
  pauseBtn.addEventListener('click', () => {
    isPaused = !isPaused;
    pauseBtn.textContent = isPaused ? 'Play' : 'Pause';
    pauseBtn.dataset.paused = isPaused ? false : true;
    console.log(pauseBtn.dataset.paused)
  });
  

// Variables pour la rotation
let isDragging = false;
let previousMousePosition = {
  x: 0,
  y: 0,
};
let rotation = {
  x: mesh.rotation.x,
  y: mesh.rotation.y,
};

const speedRange = document.getElementById("speedRange");
let degreesPerPixel = 0.01; // Valeur initiale

// Mettre à jour la valeur de degreesPerPixel lorsque le curseur est modifié
speedRange.addEventListener("input", () => {
  degreesPerPixel = parseFloat(speedRange.value);
});

// Fonction pour effectuer la rotation
const rotateCube = (event) => {
  if (isDragging) {
    let deltaMove = {
      x: event.offsetX - previousMousePosition.x,
      y: event.offsetY - previousMousePosition.y,
    };
    // console.log(deltaMove);

    // Rotation en Y
    if (mesh.rotation.x <= -Math.PI / 2 || mesh.rotation.x >= Math.PI / 2) {
      console.log("toto");
      mesh.rotation.y -= (degreesPerPixel * deltaMove.x) % (2 * Math.PI);
    } else {
      mesh.rotation.y += (degreesPerPixel * deltaMove.x) % (2 * Math.PI);
    }

    // Rotation en X
    mesh.rotation.x += (degreesPerPixel * deltaMove.y) % (2 * Math.PI);

    previousMousePosition = {
      x: event.offsetX,
      y: event.offsetY,
    };
  }
};

// Ecouteurs d'événements pour la rotation
canvas.addEventListener(
  "mousedown",
  function (event) {
    isDragging = true;
    previousMousePosition = {
      x: event.offsetX,
      y: event.offsetY,
    };
  },
  false
);

canvas.addEventListener("mousemove", rotateCube, false);

canvas.addEventListener(
  "mouseup",
  function (event) {
    isDragging = false;
  },
  false
);

canvas.addEventListener(
  "mouseout",
  function (event) {
    isDragging = false;
  },
  false
);

const zoomFactor = 0.5; // Facteur de zoom

// Ecouteur d'événement pour la molette de la souris
canvas.addEventListener('wheel', (event) => {
  const zoomDirection = event.deltaY > 0 ? 1 : -1; // Direction de la molette (vers le haut ou vers le bas)
  camera.position.z += zoomFactor * zoomDirection; // Ajustement de la position de la caméra en fonction de la direction de la molette et du facteur de zoom
});

// Animation
const update = () => {
  renderer.render(scene, camera);
  window.requestAnimationFrame(update);
};

update();

// Déplacement automatique sur l'axe x ou y
const tick = () => {
  // Time
  const currentTime = Date.now();
  const deltaTime = currentTime - time;
  time = currentTime;
//   console.log(deltaTime);

  // Update objects
  if(!isPaused){
    mesh.rotation.y -= 0.0001 * deltaTime;
  mesh.rotation.x -= 0.0005 * deltaTime;
  }
  

  // Render
  renderer.render(scene, camera);
  window.requestAnimationFrame(tick);
};

tick();



