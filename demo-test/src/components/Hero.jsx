import { useAnimations, useGLTF, useKeyboardControls } from "@react-three/drei";
import { useFrame, useLoader } from "@react-three/fiber";
import { useControls } from "leva";
import { useEffect, useRef, useState } from "react";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import * as THREE from "three";

const Hero = (props) => {
  const hero = useLoader(GLTFLoader, "./models/Hero/test.glb ");
  const heroRef = useRef();
  console.log("hero", hero);

  // hero.scene.children.forEach((mesh, i) => {
  //   mesh.castShadow = true;
  // });
  // hero.castShadow = true;
  // hero.scene.castShadow = true;

  const [forward, setForward] = useState(false);
  let delta = 0.04;
  const [angle, setAngle] = useState(-Math.PI/2);
  const animations = useAnimations(hero.animations, hero.scene);

  // Control
  const [subscribeKeys, getKeys] = useKeyboardControls();

  const { animationName } = useControls({
    animationName: { options: animations.names },
  });
  let action = animations.actions["Walk"];

  // Movement
  const moveRight = (delta) => {
    // heroRef.current.position.x -= delta * -angle;
    heroRef.current.rotation.y += delta * Math.PI * 0.5;
    const newAngle = (heroRef.current.rotation.y) / (Math.PI)  ;
    console.log("angle",newAngle)
    setAngle(newAngle);
  };

  const moveTop = (delta, angle) => {
    console.log("2", angle);
    const moveX = Math.cos(angle) * delta;
    const moveZ = Math.sin(angle) * delta;
    heroRef.current.position.x += moveX;
    heroRef.current.position.z += moveZ;
    console.log("cos, sin", moveX, moveZ);

    // heroRef.current.rotation.y = delta * Math.PI * 2 * angle;
  };

  useEffect(() => {
    return subscribeKeys(
      (state) => state.forward,
      (pressed) => {
        if (pressed) {
          const action = animations.actions["Run"];
          action.reset().fadeIn(0.5).play();
          setForward(true);
        } else {
          const action = animations.actions["Run"];
          action.reset().fadeOut(0.5).play();
          setForward(false);
        }
      }
    );

    // action = animations.actions[animationName];
    // action.reset().fadeIn(0.5).play();
    // return () => {
    //   action.fadeOut(0.5);
    // };
  }, [animations.actions, subscribeKeys, getKeys, angle]);

  useEffect(() => {
    return subscribeKeys(
      (state) => {
        console.log("state", state);
        return state.backward;
      },
      (pressed) => {
        if (pressed) {
          console.log("pressed", animations.actions);
          const action = animations.actions["Walk"];
          action.reset().fadeIn(0.5).play();
          setForward(true);
        } else {
          const action = animations.actions["Walk"];
          action.reset().fadeOut(0.5).play();
          setForward(false);
        }
      }
    );
  }, [animations.actions, subscribeKeys, getKeys]);

  useFrame(() => {
    const { forward, backward, leftward, rightward } = getKeys();

    if (rightward) moveRight(-delta);
    if (leftward) moveRight(delta);
    if (forward) moveTop(-delta, angle);
    if (backward) moveTop(delta, angle);
  });

  const direction = new THREE.Vector3();
  const targetRotation = new THREE.Quaternion();
  const lerpAmount = 0.1; // la vitesse de l'interpolation
  const speed = 0;
  // useFrame(({ delta }) => {
  //   const { forward, backward, leftward, rightward } = getKeys();

  //   const angle = heroRef.current.rotation.y;

  //   direction.set(0, 0, forward ? -1 : backward ? 1 : 0);
  //   direction.applyQuaternion(heroRef.current.quaternion);

  //   const moveDirection = new THREE.Vector3();
  //   if (leftward) moveDirection.add(new THREE.Vector3(-1, 0, 0));
  //   if (rightward) moveDirection.add(new THREE.Vector3(1, 0, 0));
  //   if (forward || backward) {
  //     moveDirection.copy(direction);
  //   }
  //   moveDirection.normalize();

  //   heroRef.current.position.addScaledVector(moveDirection, delta * speed);

  //   // Interpolation linéaire de la rotation actuelle vers la direction dans laquelle le personnage se déplace
  //   targetRotation.setFromAxisAngle(
  //     new THREE.Vector3(0, 1, 0),
  //     Math.atan2(direction.x, direction.z)
  //   );
  //   heroRef.current.quaternion.slerp(targetRotation, lerpAmount);
  // });

  // useFrame((state, delta) => {
  //   const speed = 2;
  //   const distance = speed * delta;
  //   if (forward) {
  //     hero.scene.position.z += distance;
  //   }
  //   if (forward) {
  //     hero.scene.position.z -= distance;
  //   }
  // });

  return (
    <>
    <group {...props}>
      <mesh castShadow>
        <primitive ref={heroRef} object={hero.scene} position={[0, 0, 0]} />
      </mesh>
    </group>
      
    </>
  );
};

export default Hero;
