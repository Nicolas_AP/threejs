import { useAnimations, useGLTF } from "@react-three/drei";
import { useEffect } from "react";
import { useControls } from "leva";
import { SphereGeometry } from "three";

const Ball = () => {
  const fox = useGLTF("./models/Fox/glTF/Fox.gltf");
  const animations = useAnimations(fox.animations, fox.scene);

  const sphere = new SphereGeometry(3,16,16)
  // const { animationName } = useControls({
  //   animationName: { options: animations.names },
  // });

  // useEffect(() => {
  //   const action = animations.actions[animationName];
  //   action.reset().fadeIn(0.5).play();
  //   return () => {
  //     action.fadeOut(0.5);
  //   };
  // }, [animationName]);

  return (
    <>
    <mesh>

      <primitive
        object={sphere}
        // scale={0.02}
        // position={[2.5, 0, 2.5]}
        // rotation-y={0.3}
        />
        <meshStandardMaterial color={"pink"} />
        </mesh>
    </>
  );
};

export default Ball;
