import {
  BakeShadows,
  Float,
  MeshReflectorMaterial,
  OrbitControls,
  PerspectiveCamera,
  PresentationControls,
  Stage,
  useHelper,
} from "@react-three/drei";
import * as THREE from "three";

import { useLoader } from "@react-three/fiber";
import { Suspense, useRef } from "react";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import Fox from "./Fox";
import Hero from "./Hero";
import Ball from "./Ball";

const Experience = () => {
  const directionalLight = useRef();
  useHelper(directionalLight, THREE.DirectionalLightHelper, 5);
  return (
    <>
      <axesHelper args={[8]} />
      <OrbitControls makeDefault />
      <directionalLight
        ref={directionalLight}
        castShadow
        position={[1, 6, 6]}
        intensity={1.5}
      />
      <ambientLight intensity={0.5} />
      <spotLight intensity={1} />

      {/* <mesh>
      <axesHelper args={[8]} />
        <Hero scale={1.5}/>
      </mesh> */}

      {/* <mesh castShadow>
        <Fox />
      </mesh> */}

      <mesh castShadow position-x={4} position-y={1}>
        <sphereGeometry />
        <meshStandardMaterial color={"blue"} />
      </mesh>

      <mesh receiveShadow position-y={0} rotation-x={-Math.PI * 0.5} scale={20}>
        <planeGeometry />
        
        <meshStandardMaterial color="grey" />
      </mesh>

      <Ball />
    </>
  );
};

export default Experience;
